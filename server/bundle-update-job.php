<?php

	$user_id = '';
	if (isset($_POST['user_id'])) {
		$user_id = $_POST['user_id'];
	}

	$job_id = 0;
	if (isset($_POST['job_id'])) {
		$job_id = intval($_POST['job_id']);
	}

	$exit_status = -1;
	if (isset($_POST['exit_status'])) {
		$exit_status = intval($_POST['exit_status']);
	}

	$bundle_uid = '';
	if (isset($_GET['bundle_uid'])) {
		$bundle_uid = $_GET['bundle_uid'];
	}

	system("python3 python/bundle-update-job.py '" . dirname(__FILE__) . "/databases/bundles.db' " .
	       " '" . dirname(__FILE__) . "/data' '$bundle_uid' '$user_id' '$job_id' '$exit_status'");

?>
