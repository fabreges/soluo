<?php

	$user_id = '';
	if (isset($_POST['user_id'])) {
		$user_id = base64_encode($_POST['user_id']);
	}

	$architecture = '';
	if (isset($_POST['architecture'])) {
		$architecture = base64_encode($_POST['architecture']);
	}

	$token = 0;
	if (isset($_POST['token'])) {
		$token = intval($_POST['token']);
	}

	$chunk_size = 2097152;
	if (isset($_POST['chunk_size'])) {
		$chunk_size = intval($_POST['chunk_size']);
		if ($chunk_size <= 1000) {
			$chunk_size = 1000;
		} else if ($chunk_size > 10485760) {
			$chunk_size = 10485760;
		}
	}

	$bundle_uid = '';
	if (isset($_GET['bundle_uid'])) {
		$bundle_uid = base64_encode($_GET['bundle_uid']);
	}

	system("python3 python/bundle-download.py '" . dirname(__FILE__) . "/databases/bundles.db' " .
	       " '" . dirname(__FILE__) . "/data' '$bundle_uid' '$user_id' '$architecture' '$token' '$chunk_size'");

?>
