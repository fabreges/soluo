<?php

	$user_id = '';
	if (isset($_POST['user_id'])) {
		$user_id = base64_encode($_POST['user_id']);
	}

	$cores = 1;
	if (isset($_POST['cores'])) {
		$cores = intval($_POST['cores']);
		if ($cores < 1) {
			$cores = 1;
		}
	}

	$bundle_uid = '';
	if (isset($_GET['bundle_uid'])) {
		$bundle_uid = base64_encode($_GET['bundle_uid']);
	}

	system("python3 python/bundle-registration.py '" . dirname(__FILE__) . "/databases/bundles.db' '$bundle_uid' '$user_id' '$cores'");

?>
