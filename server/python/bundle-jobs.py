import base64
import datetime
import json
import pathlib
import random
import sys

from database import DB

db_path    = sys.argv[1]
bundle_dir = pathlib.Path(sys.argv[2])
bundle_uid = base64.b64decode(sys.argv[3]).decode()
user_id    = base64.b64decode(sys.argv[4]).decode()
count      = int(sys.argv[5])
PATIENCE   = 1.5 * 60 * 60

try:
    with DB(db_path) as cursor:

        res = cursor.execute("SELECT id FROM bundles WHERE uid = ? LIMIT 1", (bundle_uid, )).fetchone()
        if res is None:
            raise Exception(f"bundle with UID '{bundle_uid}' does not exist")

        bundle_id = res['id']
        res       = cursor.execute("SELECT * FROM registrations WHERE bundle_id = ? AND user_id = ? LIMIT 1", (bundle_id, user_id)).fetchone()

        if res is None:
            raise Exception(f"user '{user_id}' is not registered for bundle '{bundle_uid}'")

        cores = res['cores']


    bundle_db = bundle_dir / bundle_uid / "tasks.db"
    with DB(bundle_db) as cursor:
        all_tasks = [{"id": x['id'],
                      "finished": (None if x['finished'] is None else datetime.datetime.fromisoformat(x['finished'])),
                      "assigned_on": (None if x['assigned_on'] is None else datetime.datetime.fromisoformat(x['assigned_on'])),
                      "assigned_to": x['assigned_to']} for x in cursor.execute("SELECT * FROM tasks")]

    # Making sure that datetime object is on the same
    # timezone (UTC0) as the database while being offset naive
    now = datetime.datetime.fromisoformat(
            datetime.datetime.now(
                tz = datetime.timezone(
                    datetime.timedelta(seconds = 0))).strftime('%Y-%m-%d %H:%M:%S'))
    todo = [x for x in all_tasks if (x['finished'] is None and x['assigned_to'] is None)
                                 or (x['finished'] is None and (now - x['assigned_on']).total_seconds() > PATIENCE)]

    done = [x for x in all_tasks if (x['finished'] is not None)]
    
    count_todo    = len(todo)
    count_done    = len(done)
    count_running = len(all_tasks) - count_todo - count_done

    with DB(bundle_db) as cursor:
        selected = []
        for i in range(count if count > 0 else (cores * 10)):
            if len(todo) == 0:
                break

            #idx = 0
            idx = random.randint(1, len(todo)) - 1
            selected += [todo[idx]['id']]
            del todo[idx]

            cursor.execute(
                    "UPDATE tasks SET assigned_to = ?, assigned_on = CURRENT_TIMESTAMP WHERE id = ?",
                    (user_id, selected[-1]))


    obj = {'jobs': selected, 'done': count_done, 'todo': count_todo, 'running': count_running}


except Exception as ex:
    obj = {'status': 'error', 'description': f'an unexpected error occured: {ex}'}

print(json.dumps(obj))
