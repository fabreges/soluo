import json
import sys

from database import DB

with DB(sys.argv[1]) as cursor:

    req = cursor.execute("SELECT uid,name,description,owner,creation FROM bundles WHERE finished IS NULL")
    res = req.fetchall()

    obj = {'bundles': [{k:x[k] for k in x.keys()} for x in res]}

print(json.dumps(obj))
