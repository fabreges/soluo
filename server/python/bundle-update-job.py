import json
import pathlib
import random
import sys

from database import DB

db_path     = sys.argv[1]
bundle_dir  = pathlib.Path(sys.argv[2])
bundle_uid  = sys.argv[3]
user_id     = sys.argv[4]
job_id      = int(sys.argv[5])
exit_status = int(sys.argv[6])

try:
    with DB(db_path) as cursor:

        res = cursor.execute("SELECT id FROM bundles WHERE uid = ? LIMIT 1", (bundle_uid, )).fetchone()
        if res is None:
            raise Exception(f"bundle with UID '{bundle_uid}' does not exist")

        bundle_id = res['id']
        res       = cursor.execute("SELECT * FROM registrations WHERE bundle_id = ? AND user_id = ? LIMIT 1", (bundle_id, user_id)).fetchone()

        if res is None:
            raise Exception(f"user '{user_id}' is not registered for bundle '{bundle_uid}'")

        cores = res['cores']


    bundle_db = bundle_dir / bundle_uid / "tasks.db"
    with DB(bundle_db) as cursor:
        if exit_status == 0:
            cursor.execute("UPDATE tasks SET finished = CURRENT_TIMESTAMP WHERE id = ?", (job_id, ))
        else:
            cursor.execute("UPDATE tasks SET assigned_to = NULL, assigned_on = NULL WHERE id = ?", (job_id,))

        count_done = cursor.execute("SELECT COUNT(*) FROM tasks WHERE finished IS NOT NULL").fetchone()[0]

    obj = {'status': 'ok', 'description': 'job updated successfully', 'done': count_done, 'cores': cores}


except Exception as ex:
    obj = {'status': 'error', 'description': f'an unexpected error occured: {ex}'}

print(json.dumps(obj))
