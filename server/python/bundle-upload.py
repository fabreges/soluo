import base64
import json
import sys
import uuid

from database import DB

db_path     = sys.argv[1]
bundle_uid  = base64.b64decode(sys.argv[2]).decode()
user_id     = base64.b64decode(sys.argv[3]).decode()

upload_uid  = uuid.uuid4().hex[:12]

try:
    with DB(db_path) as cursor:

        res = cursor.execute("SELECT id FROM bundles WHERE uid = ? LIMIT 1", (bundle_uid, )).fetchone()
        if res is None:
            raise Exception(f"bundle with UID '{bundle_uid}' does not exist")

        bundle_id = res['id']
        res       = cursor.execute("SELECT * FROM registrations WHERE bundle_id = ? AND user_id = ? LIMIT 1", (bundle_id, user_id)).fetchone()
        if res is None:
            raise Exception(f"user {user_id} is not registered with bundle UID '{bundle_uid}'")

        registration_id = res['id']
        res = cursor.execute("SELECT * FROM uploads WHERE registration_id = ? LIMIT 1", (registration_id, )).fetchone()
        if res is None:
            description   = "newly created upload"
            cursor.execute(
                    "INSERT INTO uploads (uid, registration_id) VALUES(?, ?)",
                    (upload_uid, registration_id))
        else:
            upload_uid    = res['uid']
            description   = "existing upload"

        obj = {'status': 'ok', 'description': description, 'uid': upload_uid}

except Exception as ex:
    obj = {'status': 'error', 'description': f'an unexpected error occured: {ex}'}

print(json.dumps(obj))
