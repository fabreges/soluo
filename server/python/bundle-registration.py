import base64
import json
import sys

from database import DB

db_path    = sys.argv[1]
bundle_uid = base64.b64decode(sys.argv[2]).decode()
user_id    = base64.b64decode(sys.argv[3]).decode()
cores      = int(sys.argv[4])

try:
    with DB(db_path) as cursor:

        res = cursor.execute("SELECT id FROM bundles WHERE uid = ? LIMIT 1", (bundle_uid, )).fetchone()
        if res is None:
            raise Exception(f"bundle with UID '{bundle_uid}' does not exist")

        bundle_id = res['id']
        res       = cursor.execute("SELECT * FROM registrations WHERE bundle_id = ? AND user_id = ? LIMIT 1", (bundle_id, user_id)).fetchone()
        if res is not None:
            cursor.execute(
                "UPDATE registrations SET cores = ? WHERE bundle_id = ? AND user_id = ?",
                (cores, bundle_id, user_id))

            obj = {'registration': {'status': 'ok', 'description': 'already registered'}}

        else:
            cursor.execute(
                "INSERT INTO registrations (bundle_id, user_id, cores) VALUES(?, ?, ?)",
                (bundle_id, user_id, cores))

            obj = {'registration': {'status': 'ok', 'description': 'newly registered'}}

except Exception as ex:
    obj = {'registration': {'status': 'error', 'description': f'an unexpected error occured: {ex}'}}

print(json.dumps(obj))
