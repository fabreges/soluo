import base64
import hashlib
import json
import os
import pathlib
import sys

from database import DB

db_path    = sys.argv[1]
up_path    = sys.argv[2]
upload_uid = base64.b64decode(sys.argv[3]).decode()
datafile   = sys.argv[4]
md5hash    = base64.b64decode(sys.argv[5]).decode()

try:
    with DB(db_path) as cursor:

        res = cursor.execute("SELECT * FROM uploads WHERE uid = ? LIMIT 1", (upload_uid, )).fetchone()
        if res is None:
            raise Exception(f"upload with UID '{upload_uid}' not initiated")

        res = cursor.execute("""
            SELECT b.uid FROM bundles AS b
                JOIN registrations AS r ON r.bundle_id = b.id
            WHERE
                r.id = ?
            """, (res['registration_id'], )).fetchone()
        if res is None:
            raise Exception("an unknown error occured")

        with open(datafile, "r") as fr:
            chunk_data = base64.b64decode(fr.read())

        hasher = hashlib.md5()
        hasher.update(chunk_data)
        md5hash_up = hasher.hexdigest()

        if md5hash != md5hash_up:
            raise Exception("corrupted data during upload")
        
        up_path = pathlib.Path(up_path) / res['uid'] / upload_uid
        up_path.parent.mkdir(parents = True, exist_ok = True)
        with open(up_path, "ab") as fa:
            fa.write(chunk_data)

        filesize = os.stat(up_path).st_size
        obj = {'status': 'ok', 'description': 'chunk upload successful', 'uploaded_size': filesize}

except Exception as ex:
    obj = {'status': 'error', 'description': f'an unexpected error occured: {ex}'}

print(json.dumps(obj))
