import os
import re
import sqlite3
import uuid

class DB:
    """ Fast connection to the database using context management.

    :param dbpath: Filepath to the sqlite database
    :type dbpath: str
    """

    def __init__(self, dbpath = None):
        self.dbpath = ':memory:' if dbpath is None else dbpath

    def __enter__(self):
        self.conn = sqlite3.connect(self.dbpath, timeout = 60)
        self.conn.row_factory = sqlite3.Row
        cursor = self.conn.cursor()
        cursor.execute("PRAGMA foreign_keys = ON")
        return cursor

    def __exit__(self, type, value, traceback):
        self.conn.commit()
        self.conn.close()


if __name__ == "__main__":
    import argparse
    import pathlib
    import shutil
    import stat

    def pretty_print_db(rows, dbname = None):
        width, height = shutil.get_terminal_size((80, 20))
        lines = []

        topleft  = "┌"
        topright = "┐" 
        if dbname is not None:
            lines += [["┏"] + ["━"] * (width - 2) + ["┓"]]
            lines += [["┃"] + [f"{dbname.upper():^{width-2}s}"] + ["┃"]]
            topleft, topright = "┞", "┦"

        if len(rows) == 0:
            string = "*** database is empty ***"

            lines += [[topleft] + ["─"] * (width - 2) + [topright]]
            lines += [["│"] + [f"{string:^{width-2}s}"] + ["│"]]
            lines += [["└"] + ["─"] * (width - 2) + ["┘"]]

        else:
            keys    = rows[0].keys()
            keynum  = len(keys)
            iwidth  = width - 2 - (keynum - 1)

            weights = [1] * keynum
            for ki in range(keynum):
                weights[ki] = len(str(keys[ki]))
                for row in rows:
                    weights[ki] = max(weights[ki], len(str(row[keys[ki]])))

            total_weight = sum(weights)
            colwidths    = [int(round(iwidth * weights[i] / total_weight)) for i in range(keynum)]
            colwidths    = [max(3, x) for x in colwidths]
            maxwidth     = max(colwidths)
            for ki in range(keynum):
                if colwidths[ki] == maxwidth:
                    colwidths[ki] += iwidth - sum(colwidths)
                    break
            
            lines += [[]]
            for ki in range(keynum):
                lines[-1] += [topleft] if ki == 0 else ["┬"]
                lines[-1] += ["─"] * colwidths[ki]
            lines[-1] += [topright]

            lines += [[]]
            for ki in range(keynum):
                value = str(keys[ki])
                if len(value) > colwidths[ki]:
                    value = value[:(colwidths[ki]-1)] + "…"

                lines[-1] += ["│"]
                lines[-1] += [f"{value:^{colwidths[ki]}}"]
            lines[-1] += ["│"]

            lines += [[]]
            for ki in range(keynum):
                lines[-1] += ["├"] if ki == 0 else ["┼"]
                lines[-1] += ["─"] * colwidths[ki]
            lines[-1] += ["┤"]

            for row in rows:
                lines += [[]]
                for ki in range(keynum):
                    value = str(row[keys[ki]])
                    if len(value) > colwidths[ki]:
                        value = value[:(colwidths[ki]-1)] + "…"

                    lines[-1] += ["│"]
                    lines[-1] += [f"{value:^{colwidths[ki]}}"]
                lines[-1] += ["│"]

            lines += [[]]
            for ki in range(keynum):
                lines[-1] += ["└"] if ki == 0 else ["┴"]
                lines[-1] += ["─"] * colwidths[ki]
            lines[-1] += ["┘"]

        for line in lines:
            print("".join(line))



    parser = argparse.ArgumentParser()
    parser.add_argument("--dry", "-d", action = "store_true", default = False, help = "Do not attempt to modify the database.")
    parser.add_argument("--show", "-s", action = "store_true", default = False, help = "Show the content of databases.")
    parser.add_argument("--bundle-path", "-b", required = False, default = None, help = "Path to the folder storing bundles to create a new one.")
    parser.add_argument("main_db", help = "Main database of the application")
    args = parser.parse_args()

    if args.bundle_path is not None:
        print("UID of the bundle (leave empty for random): ")
        bundle_uid = input()
        bundle_uid = (bundle_uid[:12] + uuid.uuid4().hex[:12])[:12]

        bundle_path = pathlib.Path(args.bundle_path) / bundle_uid
        bundle_db   = bundle_path / "tasks.db"
        if not bundle_db.exists():
            print("Name of the bundle: ")
            bundle_name = input()
            bundle_name = "".join([x for x in bundle_name if re.match('[0-9a-zA-Z_ ()+=-]', x)])[:30]

            print("Description: ")
            bundle_description = input()
            bundle_description = "".join([x for x in bundle_description if re.match('[0-9a-zA-Z_ ()+=-]', x)])[:150]

            print("Owner's name: ")
            bundle_owner = input()
            bundle_owner = "".join([x for x in bundle_owner if re.match('[0-9a-zA-Z_ ()+=-]', x)])[:25]

            print("Number of tasks: ")
            bundle_tasks = int(input())
            if bundle_tasks < 0:
                raise ValueError("Number of tasks must be greater than 0")
            elif bundle_tasks > 10000000:
                raise ValueError("Number of tasks cannot be greater than 10 millions")


    with DB(args.main_db) as cursor:
        if not args.dry:
            cursor.execute(
            """
                CREATE TABLE IF NOT EXISTS bundles (
                    id          INTEGER       PRIMARY KEY AUTOINCREMENT,
                    uid         VARCHAR (12)  UNIQUE
                                              NOT NULL,
                    name        VARCHAR (30)  UNIQUE
                                              NOT NULL,
                    description VARCHAR (150),
                    owner       VARCHAR (25)  NOT NULL,
                    creation    DATETIME      DEFAULT (CURRENT_TIMESTAMP)
                                              NOT NULL,
                    finished    DATETIME
                );
            """)

            cursor.execute(
            """
                CREATE TABLE IF NOT EXISTS registrations (
                    id        INTEGER       PRIMARY KEY AUTOINCREMENT,
                    bundle_id INTEGER       REFERENCES bundles (id) 
                                            NOT NULL,
                    user_id   VARCHAR (255) NOT NULL,
                    creation  DATETIME      NOT NULL
                                            DEFAULT (CURRENT_TIMESTAMP),
                    cores     INTEGER       NOT NULL
                                            CHECK (cores > 0),
                    UNIQUE (
                        bundle_id,
                        user_id
                    )
                    ON CONFLICT IGNORE
                );
            """)

            cursor.execute(
            """
                CREATE TABLE IF NOT EXISTS uploads (
                    id              INTEGER       PRIMARY KEY AUTOINCREMENT,
                    uid             VARCHAR(12)   UNIQUE NOT NULL,
                    registration_id INTEGER       REFERENCES registrations (id) 
                                                  NOT NULL
                );
            """)

            if args.bundle_path is not None and not bundle_db.exists():
                try:
                    cursor.execute(
                    """
                        INSERT INTO bundles (uid, name, description, owner)
                            VALUES (?, ?, ?, ?)
                    """, (bundle_uid, bundle_name, bundle_description, bundle_owner))
                except sqlite3.IntegrityError:
                    print("warning: bundle was already registered in the bundles.db database")

    
        if args.show:
            rows_bundle = cursor.execute("SELECT * FROM bundles").fetchall()
            pretty_print_db(rows_bundle, dbname = f"{args.main_db}::bundles")

            rows_registrations = cursor.execute("SELECT * FROM registrations").fetchall()
            pretty_print_db(rows_registrations, dbname = f"{args.main_db}::registrations")

            rows_uploads = cursor.execute("SELECT * FROM uploads").fetchall()
            pretty_print_db(rows_uploads, dbname = f"{args.main_db}::uploads")


    if args.bundle_path is not None:
        if not args.dry and not bundle_db.exists():
            os.makedirs(os.path.dirname(bundle_db), exist_ok = True)
            with DB(bundle_db) as cursor:
                cursor.execute(
                """
                    CREATE TABLE tasks (
                        id          INTEGER       PRIMARY KEY
                                                  UNIQUE,
                        finished    DATETIME,
                        assigned_to VARCHAR (255),
                        assigned_on DATETIME
                    );
                """)

                for i in range(bundle_tasks):
                    cursor.execute("INSERT INTO tasks (id) VALUES (?)", (i+1, ))

            bundle_db.chmod(stat.S_IRUSR | stat.S_IWUSR | stat.S_IRGRP | stat.S_IWGRP)

        if args.show and bundle_db.exists():
            with DB(bundle_db) as cursor:
                rows_tasks = cursor.execute("SELECT * FROM tasks").fetchall()
                pretty_print_db(rows_tasks, dbname = f"{bundle_path}::tasks")

