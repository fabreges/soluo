import base64
import json
import pathlib
import sys

from database import DB

db_path      = sys.argv[1]
bundle_dir   = pathlib.Path(sys.argv[2])
bundle_uid   = base64.b64decode(sys.argv[3]).decode()
user_id      = base64.b64decode(sys.argv[4]).decode()
architecture = base64.b64decode(sys.argv[5]).decode()
token        = int(sys.argv[6])
chunk_size   = max(1, int(sys.argv[7]))

try:
    with DB(db_path) as cursor:

        res = cursor.execute(
                """
                    SELECT r.* FROM registrations AS r
                    JOIN bundles AS b ON b.id = r.bundle_id
                    WHERE b.uid = ? AND r.user_id = ? LIMIT 1
                """, (bundle_uid, user_id)).fetchone()

        if res is None:
            raise Exception(f"user '{user_id}' is not registered for bundle '{bundle_uid}'")


    filepath = bundle_dir / bundle_uid / f"bundle-{architecture}.tar.gz"

    if not filepath.exists():
        raise Exception(f"bundle data cannot be found on the server")

    content    = b''
    next_token = token
    with open(filepath, 'rb') as fr:
        fr.seek(token)
        content    = fr.read(chunk_size)
        eof        = len(fr.read(1)) == 0
        next_token = -1 if eof else token + len(content)

    obj = {'status': 'ok', 'chunk': base64.b64encode(content).decode(), 'token': next_token, 'read': next_token, 'total': filepath.stat().st_size}       

except Exception as ex:
    obj = {'status': 'error', 'description': f'an unexpected error occured: {ex}'}

print(json.dumps(obj))
