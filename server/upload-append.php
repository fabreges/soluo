<?php

	$md5hash = '';
	if (isset($_POST['md5hash'])) {
		$md5hash = base64_encode($_POST['md5hash']);
	}

	$datafile = null;
	if (isset($_POST['data'])) {
		$datafile = tempnam(sys_get_temp_dir(), 'soluo');
		$fd = fopen($datafile, 'w');
		fwrite($fd, $_POST['data']);
		fclose($fd);
	}

	$upload_uid = '';
	if (isset($_GET['upload_uid'])) {
		$upload_uid = base64_encode($_GET['upload_uid']);
	}

	system("python3 python/upload-append.py '" . dirname(__FILE__) . "/databases/bundles.db'" .
		" '" . dirname(__FILE__) . "/data/uploads/' '$upload_uid' '$datafile' '$md5hash'");

	unlink($datafile)

?>
