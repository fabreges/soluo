<?php

	$user_id = '';
	if (isset($_POST['user_id'])) {
		$user_id = base64_encode($_POST['user_id']);
	}

	$count = 0;
	if (isset($_POST['count'])) {
		$count = intval($_POST['count']);
		if ($count < 0 ) {
			$count = 0;
		}
	}

	$bundle_uid = '';
	if (isset($_GET['bundle_uid'])) {
		$bundle_uid = base64_encode($_GET['bundle_uid']);
	}

	system("python3 python/bundle-jobs.py '" . dirname(__FILE__) . "/databases/bundles.db' " .
	       " '" . dirname(__FILE__) . "/data' '$bundle_uid' '$user_id' $count");

?>
