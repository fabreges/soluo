# Soluo

Soluo is a tool to distribute the computational load on multiple computers, thus providing
a virtual cluster of computer scattered around the world.

## Client

### Installation

```bash
cd <folder>
git clone https://gitlab.com/fabreges/soluo.git ./soluo
cd soluo
conda env create -n soluo -f environment.yml
```

Conda is required to use soluo.


### Running

```bash
cd <folder>
cd soluo
conda activate soluo
python -m soluo <remote host>
```

## Server

Soluo must be hosted on a central server available by every client to work efficiently.
The webserver must run PHP and allow system calls to Python3.

### Installation

```bash
cd <folder>
git clone https://gitlab.com/fabreges/soluo.git ./soluo

# Initialize main databases
cd soluo/
python3 server/python/database.py --show server/databases/bundles.db
```

### Adding a new bundle

#### Pre-requisit
A bundle must contains those minimum files:

- jobs
- environment.yml

`jobs` contains a list of command to be executed by the client. Paths must be relative to
the bundle's directory.

`environment.yml` gives the list of required packages for the conda environment used for computing.

Commands listed in `jobs` (one per line) must save the output in a folder `<bundle>/output'.
The logs (stdout and stderr) will be automatically stored in a folder '<bundle>/logs'.

#### Create an archive of the bundle

```bash
tar -cvzf bundle.tar.gz -C <bundle_dir> .
```

The archive must be named according to the targetted architecture.

- bundle-arm64.tar.gz (e.g., M1 chip maxOS)
- bundle-x86_64.tar.gz (e.g., Intel chip macOS, Unix, ...)
- bundle-amd64.tar.gz (e.g., Windows AMD processor)

This system must be changed in the future to make sure the right code is run on the right machine.

#### Register a bundle

On the server, register the bundle in the database. Run the following commands and follow
the instructions:

```bash
cd <folder>
cd soluo/server
python3 python/database.py --show --bundle-path data/ databases/bundles.db
```

#### Deliver the bundle

Move the bundle's archives to the bundle folder.
The bundle folder should be in `<folder>/soluo/server/data/<bundle_uid>`

