import base64
import hashlib
import json
import os
import requests
import time

class API:

    STATUS_OK = 'ok'

    def __init__(self, remote, *, retry_on_error = 3, retry_period = 60):
        """
        API to communicate with soluo remote server.

        remote: the remote host
        retry_on_error: number of times to retry if the server can't be reached
        retry_period: number of seconds to wait between attempts in case of errors
        """
        self.remote   = remote
        self.settings = {"retry_on_error": retry_on_error,
                         "retry_period":   retry_period}


    def __get(self, url, data = None):
        return self.__request(requests.get, url, data)


    def __post(self, url, data = None):
        return self.__request(requests.post, url, data)


    def __request(self, method, url, data = None):
        attempt = 0
        _ex     = None
        while attempt <= self.settings['retry_on_error']:
            try:
                return method(url, data = data)
            except requests.exceptions.ConnectionError as ex:
                _ex = ex
                attempt += 1
                time.sleep(self.settings['retry_period'])

        raise _ex


    def bundles(self):
        weburl  = f"{self.remote}/bundles"
        webdata = self.__get(weburl)
        if webdata.status_code == 200:
            try:
                return json.loads(webdata.content)['bundles']

            except json.decoder.JSONDecodeError as ex:
                raise SoluoException(f"error: illegal reply from remote host: {ex}", ex)

            except Exception as ex:
                raise SoluoException("error: malformed data", ex)

        elif webdata.status_code == 404:
            raise SoluoException(f"error: remote host is not a valid Soluo host: {self.remote}", None)

        else:
            raise SoluoException(f"error: remote host replied with an error: {webdata.status_code}", None)


    def registration(self, *, bundle_uid, user_id, cores):
        weburl  = f"{self.remote}/bundles/{bundle_uid}/registration"
        webdata = self.__post(weburl, data = { "user_id": user_id, "cores": cores })
        if webdata.status_code == 200:
            try:
                return json.loads(webdata.content)['registration']

            except json.decoder.JSONDecodeError as ex:
                raise SoluoException(f"error: illegal reply from remote host: {ex}", ex)

            except Exception as ex:
                raise SoluoException("error: malformed data", ex)

        raise SoluoException(f"error: remote host replied with an error: {webdata.status_code}", None)


    def download(self, *, bundle_uid, user_id, architecture, token, size = 2097152):
        weburl  = f"{self.remote}/bundles/{bundle_uid}/download"
        webdata = self.__post(weburl, data = { "user_id": user_id, "architecture": architecture,
                                                "token": 0 if token is None else token,
                                                "chunk_size": size })

        if webdata.status_code == 200:
            try:
                jsondata = json.loads(webdata.content)
                if jsondata['status'] != API.STATUS_OK:
                    raise SoluoException(
                        f"error: unable to download a bundle chunk from remote host: {jsondata['description']}", None)

                jsondata['chunk'] = base64.b64decode(jsondata['chunk'])
                jsondata['token'] = None if int(jsondata['token']) < 0 else int(jsondata['token'])
                return jsondata

            except json.decoder.JSONDecodeError as ex:
                raise SoluoException(f"error: illegal reply from remote host: {ex}", ex)

            except SoluoException as ex:
                raise ex

            except Exception as ex:
                raise SoluoException("error: malformed data", ex)

        raise SoluoException(f"error: remote host replied with an error: {webdata.status_code}", None)


    def upload(self, *, bundle_uid, user_id, file, size = 2097152, callback = None):
        weburl  = f"{self.remote}/bundles/{bundle_uid}/upload"
        webdata = self.__post(weburl, data = { "user_id": user_id })

        if webdata.status_code == 200:
            try:
                jsondata = json.loads(webdata.content)
                if jsondata['status'] != API.STATUS_OK:
                    raise SoluoException(
                        f"error: unable to upload results to remote host: {jsondata['description']}", None)

                if callback is not None:
                    callback(0, os.stat(file).st_size)

                return self._upload_in_chunks(
                        upload_uid = jsondata['uid'], file = file,
                        size = size, callback = callback)

            except json.decoder.JSONDecodeError as ex:
                raise SoluoException(f"error: illegal reply from remote host: {ex}", ex)

            except SoluoException as ex:
                raise ex

            except Exception as ex:
                raise SoluoException("error: malformed data", ex)

        raise SoluoException(f"error: remote host replied with an error: {webdata.status_code}", None)


    def _upload_in_chunks(self, *, upload_uid, file, size = 2097152, callback = None):
        weburl  = f"{self.remote}/uploads/{upload_uid}"

        filesize = os.stat(file).st_size
        with open(file, "rb") as fr:
            total_size = 0
            error      = 0
            last_error = None
            while error < 10:
                chunk_data = fr.read(size)
                chunk_size = len(chunk_data)
                if chunk_size == 0:
                    last_error = None
                    break

                hasher = hashlib.md5()
                hasher.update(chunk_data)
                md5hash = hasher.hexdigest()

                webdata = self.__post(weburl, data = { "md5hash": md5hash,
                                                       "data": base64.b64encode(chunk_data).decode() })

                if webdata.status_code != 200:
                    fr.seek(total_size)
                    error += 1
                    time.sleep(1)
                    last_error = SoluoException(f"error: remote host replied with an error: {webdata.status_code}", None)
                    continue

                try:
                    jsondata = json.loads(webdata.content)
                    if jsondata['status'] != API.STATUS_OK:
                        fr.seek(total_size)
                        error += 1
                        time.sleep(1)
                        last_error = SoluoException(
                                f"error: unable to upload results chunk to remote host: {jsondata['description']}", None)
                        continue

                except json.decoder.JSONDecodeError as ex:
                    fr.seek(total_size)
                    error += 1
                    time.sleep(1)
                    last_error = SoluoException(f"error: illegal reply from remote host: {ex}", ex)
                    continue

                except Exception as ex:
                    fr.seek(total_size)
                    error += 1
                    time.sleep(1)
                    last_error = SoluoException("error: malformed data", ex)
                    continue

                total_size += chunk_size
                if callback is not None:
                    callback(total_size, filesize)

        if last_error is not None:
            raise last_error


    def jobs(self, *, bundle_uid, user_id, count = 0):
        weburl  = f"{self.remote}/bundles/{bundle_uid}/jobs"
        webdata = self.__post(weburl, data = { "user_id": user_id, "count": count })
        if webdata.status_code == 200:
            try:
                json_data = json.loads(webdata.content)
                if json_data.get('jobs', None) is not None:
                    return json_data
                elif json_data['status'] == 'error':
                    raise SoluoException(json_data['description'], None)

            except json.decoder.JSONDecodeError as ex:
                raise SoluoException(f"error: illegal reply from remote host: {ex}", ex)

            except Exception as ex:
                raise SoluoException("error: malformed data", ex)

        raise SoluoException(f"error: remote host replied with an error: {webdata.status_code}", None)


    def update_job(self, *, bundle_uid, user_id, job_id, exit_status):
        weburl  = f"{self.remote}/bundles/{bundle_uid}/update-job"
        webdata = self.__post(weburl, data = { "user_id": user_id, "job_id": job_id, "exit_status": exit_status })
        if webdata.status_code == 200:
            try:
                return json.loads(webdata.content)

            except json.decoder.JSONDecodeError as ex:
                raise SoluoException(f"error: illegal reply from remote host: {ex}", ex)

            except Exception as ex:
                raise SoluoException("error: malformed data", ex)

        raise SoluoException(f"error: remote host replied with an error: {webdata.status_code}", None)


class SoluoException(Exception):
    def __init__(self, message, source):
        super().__init__(message)
        self.source  = source

    def __str__(self):
        string = super().__str__()
        if self.source is not None:
            string += "\n"
            for line in [x for x in str(self.source).split("\n") if x]:
                string += f"    {line}\n"

        return string
