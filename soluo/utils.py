import datetime
import os
import pathlib
import subprocess
import sys

def localdir():
    if sys.platform.startswith("win"):
        os_path = os.getenv("LOCALAPPDATA")

    elif sys.platform.startswith("darwin"):
        os_path = "~/Library/Application Support"

    else:
        os_path = os.getenv("XDG_DATA_HOME", "~/.local/share")

    app_path = pathlib.Path(os_path) / "soluo"

    return app_path.expanduser()


def progressbar(progress, width = 80, suffix = "{pcent:.2%}"):
    formated_suffix = suffix.format(pcent = progress)
    available_width = max(3, width - len(formated_suffix) - 2)

    left = int((progress * available_width) // 1)
    right = available_width - left

    return "".join(["["] + ["#"] * left + ["-"] * right + ["]"] + [suffix.format(pcent = progress)])


def install_dependencies(dependencies, conda_env = None, conda_exe = "conda"):
    if sys.platform.startswith("win"):
        # We expect to be in a conda terminal already
        path_to_env = str(dependencies).replace(" ", "^ ")
        run_shell_command(f"{conda_exe} env create -n {conda_env} -f {path_to_env} ")

    else:
        run_shell_command('eval "$(conda \'shell.bash\' \'hook\' 2>/dev/null)"; ' +
                         f"{conda_exe} env create -n {conda_env} -f '{str(dependencies)}'; ",
                          stdout = sys.stdout, stderr = sys.stderr)


def is_conda_installed():
    res = run_shell_command("conda")
    return res == 0

def is_mamba_installed():
    res = run_shell_command("mamba")
    return res == 0

def run_shell_command(commands, sync = True, conda_env = None, wdir = ".", stdout = None, stderr = None):

    if conda_env is not None:
        if sys.platform.startswith("win"):
            # We expect to be in a conda terminal already
            wdir = wdir.replace(" ", "^ ")
            commands = (f"cd {wdir} && " +
                        f"conda activate {conda_env} && " +
                        commands)

        else:
            commands = (f"cd '{wdir}'; " +
                        'eval "$(conda \'shell.bash\' \'hook\' 2>/dev/null)"; ' +
                        f"conda activate {conda_env}; " +
                        commands)

    j = Job(commands)
    j.start(stdout = stdout, stderr = stderr)

    if sync:
        j.process.wait()
        j.update_status()
        return j.exit

    return j


class Job:

    SHELL_UNIX     = ['/usr/bin/env', 'bash', '-c']
    SHELL_WIN      = ['C:\\Windows\\System32\\cmd.exe', '/C']

    STATUS_WAITING = 'waiting'
    STATUS_RUNNING = 'running'
    STATUS_DONE    = 'done'

    def __init__(self, commands):
        self.commands = commands
        self.status   = Job.STATUS_WAITING
        self.pid      = None
        self.exit     = None
        self.process  = None
        self.start_at = None
        self.last_op  = None
        self.end_at   = None

        if sys.platform.startswith("win"):
            self.shell = Job.SHELL_WIN
        else:
            self.shell = Job.SHELL_UNIX


    def start(self, stdout = None, stderr = None):
        if self.status != Job.STATUS_WAITING:
            raise RuntimeError("Job cannot be run twice.")

        self.__stdout = subprocess.DEVNULL if stdout is None else (None if stdout == sys.stdout else open(stdout, 'w'))
        self.__stderr = subprocess.DEVNULL if stderr is None else (None if stderr == sys.stderr else open(stderr, 'w'))

        self.process  = subprocess.Popen([*self.shell, self.commands], stdout = self.__stdout, stderr = self.__stderr)
        self.pid      = self.process.pid
        self.status   = Job.STATUS_RUNNING
        self.start_at = datetime.datetime.now()
        self.last_op  = self.start_at


    def update_status(self):
        if self.status != Job.STATUS_RUNNING:
            return

        try:
            self.process.wait(0.01)
        except subprocess.TimeoutExpired:
            pass

        if self.process.returncode is not None:
            self.status  = Job.STATUS_DONE
            self.pid     = None
            self.exit    = self.process.returncode
            self.process = None
            self.end_at  = datetime.datetime.now()

            try:
                self.__stdout.close()
                self.__stderr.close()
            except:
                pass
            finally:
                self.__stdout = None
                self.__stderr = None

            return


    def coolkill(self):
        if self.status != Job.STATUS_RUNNING:
            return

        if self.kill_attempts < 3:
            self.process.terminate()
        else:
            self.process.kill()

        self.kill_attempts += 1

