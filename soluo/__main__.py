import argparse
import multiprocessing
import os
import pick
import platform
import uuid
import shutil
import signal
import socket
import sys
import tarfile
import time

from .api import API, SoluoException
from . import utils
from .utils import Job

""" ###################
    --- 1/ Introduction
    ################### """

# Trap Ctrl+C signal
def abord(sig, frame):
    global abortion_flag

    if is_running and not abortion_flag:
        print("Soluo will quit as soon as possible. Repeat Ctrl+C to quit immediately.")
        abortion_flag = True
    elif is_running and abortion_flag:
        print("Quitting without tidying up the mess...")
        sys.exit(1)
    elif not is_running:
        print("Quitting now")
        sys.exit(1)

is_running = False
abortion_flag = False
signal.signal(signal.SIGINT, abord)

parser = argparse.ArgumentParser()
parser.add_argument("remote", help = "URL of the remote host")
args = parser.parse_args()

print("")
print("")
print("  ██████  ██████  ██      ██    ██  ██████       ██    ███████ ")
print("  ██     ██    ██ ██      ██    ██ ██    ██     ███         ██ ")
print("  ██████ ██    ██ ██      ██    ██ ██    ██      ██        ██  ")
print("      ██ ██    ██ ██      ██    ██ ██    ██      ██       ██   ")
print("  ██████  ██████  ███████  ██████   ██████       ██ ██    ██   ")
print("")
print("")

root = utils.localdir()
arch = platform.machine().lower()
if not (root / "user_id").exists():
    root.mkdir(parents = True, exist_ok = True)
    with open(root / "user_id", "w") as fw:
        fw.write(socket.gethostname() + uuid.uuid4().hex)

with open(root / "user_id", "r") as fr:
    user_id = fr.read()

print("")
print("Welcome in Soluo, a simple tool to take advantage of the")
print("power of many computers to perform simple distributed tasks.")
print(f"This computer UID is {user_id} with architecture {arch}")

has_conda = utils.is_conda_installed()
has_mamba = utils.is_mamba_installed()
if not has_conda and not has_mamba:
    print("")
    print("Soluo needs conda to work, but it cannot be found.")
    print("Make sure conda is installed and available in a standard shell.")
    sys.exit(1)

conda_exe = "conda"
if has_mamba:
    conda_exe = "mamba"

""" ############################
    --- 2/ Get available bundles
    ############################ """

print("")
print("Soluo is now gathering the available jobs from remote:")
print(args.remote)

soluo_api = API(args.remote)

try:
    bundles = soluo_api.bundles()
except SoluoException as ex:
    print(ex, file = sys.stderr)
    sys.exit(1)

if len(bundles) == 0:
    print("🤷 no bundles available: there is nothing to do")
    sys.exit(0)

""" ################
    --- 3/ Selection
    ################ """

longest_name = max([len(x['name']) for x in bundles])
bundle_options  = [f"{x['name']:<{longest_name}s} : {x['description']} (by {x['owner']})" for x in bundles]
bundle_options += ["quit"]

input("Press Enter to continue...")
_, selection = pick.pick(bundle_options, "Select a bundle for which you'd like to help:")

if selection == len(bundle_options) - 1:
    sys.exit(0)

bundle  = bundles[selection]

""" ###################
    --- 4/ Registration
    ################### """

print("")
print(f"Registration of this computer for bundle '{bundle['name']}'")
core_options = [str(i) for i in range(1, multiprocessing.cpu_count()+1)] + ["quit"]
input("Press Enter to continue...")
_, selection = pick.pick(core_options, "How many cores do you wish to share?")

if selection == len(core_options) - 1:
    sys.exit(0)

cores = selection + 1

try:
    webdata = soluo_api.registration(bundle_uid = bundle['uid'], user_id = user_id, cores = cores)
except SoluoException as ex:
    print(ex, file = sys.stderr)
    sys.exit(1)

if webdata['status'] != API.STATUS_OK:
    print(f"error: registration was not possile: {webdata['status']} ({webdata['description']})")
    sys.exit(1)

print(f"Registration done: {webdata['description']}")

""" ##################
    --- 5/ Downloading
    ################## """

print("")

destination = root / "bundles" / bundle['uid']
destination.mkdir(parents = True, exist_ok = True)

ready_flag = destination / ".ready"
if not ready_flag.exists():
    print("Soluo is now downloading the bundle. This may take a while...")

    # Make sure the folder is empty in case a download
    # was aborted earlier
    for filename in os.listdir(destination):
        filepath = destination / filename
        if filepath.is_dir() and not filepath.is_symlink():
            shutil.rmtree(filepath)
        else:
            os.unlink(filepath)

    # Download the archive
    archive = destination / ".archive.tar.gz"
    token = None
    with open(archive, "wb") as fw:
        try:
            _ = soluo_api.download(bundle_uid = bundle['uid'], user_id = user_id,
                                   architecture = arch, token = token, size = 1)
        except SoluoException as ex:
            print(f"error: unable to download the bundle for this machine architecture: {ex}")
            sys.exit(1)

        print(utils.progressbar(0, suffix = " preparing to download"), end = '\r')
        while True:
            data = soluo_api.download(bundle_uid = bundle['uid'], user_id = user_id,
                                      architecture = arch, token = token)

            fw.write(data['chunk'])

            print(utils.progressbar(data['read'] / data['total'], suffix = " {pcent:<7.2%}               "), end = '\r')

            token = data['token']
            if token is None:
                break

        print(utils.progressbar(1, suffix = " [ done ]             "))

    # Extract the archive
    with tarfile.open(archive) as ftar:
        ftar.extractall(destination)

    archive.unlink()
    open(ready_flag, "w").close()

else:
    print("Bundle has already been downloaded. If you want to force Soluo to download")
    print("a new version of the bundle, remove the following folder:")
    print(str(destination))


(destination / "output").mkdir(exist_ok = True)
(destination / "logs").mkdir(exist_ok = True)
if not (destination / "jobs").exists():
    print("")
    print("Bundle is corrupted. You may want to delete the folder and try again:")
    print(str(destination))
    sys.exit(1)


""" ##########################
    --- 6/ Prepare environment
    ########################## """

dependencies = destination / "environment.yml"
conda_env    = f"soluo_{bundle['uid']}"
print("")
print(f"Creating a dedicated conda environment: {conda_env}")
utils.install_dependencies(dependencies, conda_env = conda_env, conda_exe = conda_exe)


""" ###################
    --- 7/ Running jobs
    ################### """

jobfile  = destination / "jobs"
with open(jobfile, "r") as fr:
    commands = [x.strip() for x in fr]

is_running = True
run_round = 1
while not abortion_flag:
    try:
        jobs_schedule = soluo_api.jobs(bundle_uid = bundle['uid'], user_id = user_id)
        jobid_todo    = jobs_schedule['jobs']
        jobs_done     = jobs_schedule['done']
        jobs_total    = jobs_schedule['done'] + jobs_schedule['todo'] + jobs_schedule['running']
    except SoluoException as ex:
        print(ex, file = sys.stderr)
        sys.exit(1)

    job_count  = len(jobid_todo)
    running    = [None] * cores

    if job_count == 0:
        break

    while len(jobid_todo) > 0 and not abortion_flag:
        running = [x for x in running if x is not None]
        if len(running) < cores:
            running += [None] * (cores - len(running))

        for jid in range(len(running)):

            job = running[jid]
            if job is None or job.status == Job.STATUS_DONE:
                next_jobid, *jobid_todo = jobid_todo
                command = commands[next_jobid - 1]

                stdout = destination / "logs" / f"job_{next_jobid:09d}.stdout"
                stderr = destination / "logs" / f"job_{next_jobid:09d}.stderr"

                job = utils.run_shell_command(command, conda_env = conda_env, sync = False,
                                              wdir = str(destination), stdout = stdout, stderr = stderr)
                job._soluo_jobid = next_jobid

                running[jid] = job
                if len(jobid_todo) == 0:
                    break

            if job is not None:
                job.update_status()
                if job.status == Job.STATUS_DONE:
                    ret = soluo_api.update_job(bundle_uid = bundle['uid'], user_id = user_id,
                                               job_id = job._soluo_jobid, exit_status = job.exit)

                    if ret.get("status", None) == API.STATUS_OK:
                        jobs_done    = ret.get("done", jobs_done)
                        cores        = ret.get("cores", cores)
                        running[jid] = None

        job_running = sum([x is not None for x in running])
        job_done    = job_count - len(jobid_todo) - job_running
        progressloc = utils.progressbar(job_done / job_count, width = 40, suffix = " {pcent:.2%}" + f" ({job_done}/{job_count})")
        progressglo = utils.progressbar(jobs_done / jobs_total, width = 40, suffix = " {pcent:.2%}" + f" ({jobs_done}/{jobs_total})")
        print(f"Round {run_round:3d}: {progressloc}, total: {progressglo}", end = '\r')

        time.sleep(1)

    while len(running) > 0:
        for jid in range(len(running)):
            job = running[jid]
            if job is not None:
                job.update_status()

                if job.status == Job.STATUS_DONE:
                    ret = soluo_api.update_job(bundle_uid = bundle['uid'], user_id = user_id,
                                               job_id = job._soluo_jobid, exit_status = job.exit)

                    if ret.get("status", None) == API.STATUS_OK:
                        jobs_done = ret.get("done", jobs_done)
                        running[jid] = None

        running = [x for x in running if x is not None]

        job_running = len(running)
        job_done    = job_count - job_running
        progressloc = utils.progressbar(job_done / job_count, width = 40, suffix = " {pcent:.2%}" + f" ({job_done}/{job_count})")
        progressglo = utils.progressbar(jobs_done / jobs_total, width = 40, suffix = " {pcent:.2%}" + f" ({jobs_done}/{jobs_total})")
        print(f"Round {run_round:3d}: {progressloc}, total: {progressglo}", end = '\r')
        time.sleep(1)

    run_round += 1
    print("")

is_running = False
if abortion_flag and len(jobid_todo) > 0:
    for next_jobid in jobid_todo:
        while True:
            ret = soluo_api.update_job(bundle_uid = bundle['uid'], user_id = user_id,
                                    job_id = next_jobid, exit_status = 1)

            if ret.get("status", None) == API.STATUS_OK:
                break

            time.sleep(1)

    sys.exit(1)


""" ##################
    --- 8/ Wrapping up
    ################## """

print("")
print(f"It seems that bundle {bundle['name']} has no more jobs to do.")
print("Wrapping up the output generated on this computer, it may take a while: ", end = '', flush = True)
tarout = destination / f"{bundle['uid']}_{user_id}.tar.gz"
with tarfile.open(tarout, "w:gz") as tarfw:
    tarfw.add(destination / "output", arcname = "output")
    tarfw.add(destination / "logs", arcname = "logs")

for filename in os.listdir(destination):
    filepath = destination / filename
    if filepath == tarout:
        continue

    if filepath.is_dir() and not filepath.is_symlink():
        shutil.rmtree(filepath)
    else:
        os.unlink(filepath)

print("done!")
print("Destroying the dedicated conda environment: ", end ='', flush = True)
utils.run_shell_command(f"conda env remove -n {conda_env}")
print("done!")

""" ########################
    --- 9/ Uploading results
    ######################## """

try:
    soluo_api.upload(
            bundle_uid = bundle['uid'],
            user_id    = user_id,
            file       = tarout,
            callback   = lambda x,y: print("Upload in progress: " +
                                           utils.progressbar(x / y, suffix = " {pcent:<7.2%}"),
                                           end = '\r'))
    print("")
    print("Everything is done. The results were successfully uploaded")
    print("but you may want to keep a local copy if a problem occured.")
    print("The archive with all the results are located here:")
    print(str(tarout))

except Exception as ex:
    print("")
    print("An error occured while trying to upload the results.")
    print("A local copy has been preserved. Please do not delete")
    print("the local copy before sharing it with {bundle['owner']}.")
    print("The archive with all the results are located here:")
    print(str(tarout))
    print("")
    print("For your information, the error was the following:")
    print(ex)

print("")
print("Bye!")
